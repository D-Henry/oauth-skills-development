﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web_Api.Models;

namespace Web_Api.Services
{
    public class UserService
    {
        //Mimicing retrieving user from a database
        public User GetUserByCredentials(string email, string password)
        {
            if (email != "user@domain.com" || password != "secret")
            {
                return null;
            }

            User user = new User
            {
                Id = "1",
                Name = "John Doe",
                Email = "user@domain.com",
                EmailConfirmed = "true",
                PasswordHash = "secret"
            };
            return user; 
        }
    }
}